/* -*- Mode: C; tab-width: 4 -*- */
/* concentric, Copyright (c) 2014 David Eccles (gringer) <bioinformatics@gringene.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

/* concentric --- concentric ring extrusion module for xscreensaver
 * Notes:
 * This screensaver requires the GLE ("OpenGL Tubing and Extrusion Library")
 * which can be obtained from http://www.linas.org/gle/index.html
  */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_COCOA
# include <GLUT/tube.h>    /* gle is included with GLUT on OSX */
#else  /* !HAVE_COCOA */
# include <GL/gl.h>
# include <GL/glu.h>
# ifdef HAVE_GLE3
#  include <GL/gle.h>
# else
#  include <GL/tube.h>
# endif
#endif /* !HAVE_COCOA */


#define refresh_concentric 0

#define	DEFAULTS	"*delay:	 20000	\n" \
					"*showFPS:	 False	\n" \
					"*wireframe: False   \n"

#include "xlockmore.h"				/* from the xscreensaver distribution */
#include "rotator.h"
#include "gltrackball.h"
#include "sphere.h"
#include <sys/time.h>


#ifdef USE_GL /* whole file */

#undef countof
#define countof(x) (sizeof((x))/sizeof((*x)))

#define checkImageWidth 64
#define checkImageHeight 64

#define WIDTH 640
#define HEIGHT 480

#define PI 3.141592654

#define DEF_LIGHT	  	"True"
#define DEF_TEXTURE		"False"
#define DEF_TEX_QUAL   "False"
#define DEF_MIPMAP   	"False"
#define DEF_NAME        "RANDOM"
#define DEF_IMAGE   	"BUILTIN"

/* structure for holding the concentric data */
typedef struct {
  GLuint platelist;
  GLuint texid;            /* texture map ID */
  GLXContext *glx_context;
  int screen_width, screen_height;
  rotator *rot;
  trackball_state *trackball;
  Bool button_down_p;
  Bool button2_down_p;
  int mouse_start_x, mouse_start_y;
  int mouse_x, mouse_y;
  int mouse_dx, mouse_dy;
  Window window;
  XColor fg, bg;
  int concentric_number;
} concentric_configuration;

static concentric_configuration *Concentric = NULL;

static int do_light;
static int do_texture;
static int do_tex_qual;
static int do_mipmap;
static char *which_name;
static char *which_image;

static XrmOptionDescRec opts[] = {
  {"-light",           ".concentric.light",   XrmoptionNoArg, "true" },
  {"+light",           ".concentric.light",   XrmoptionNoArg, "false" },
  {"+texture",         ".concentric.texture", XrmoptionNoArg, "false" },
  {"-texture",         ".concentric.texture", XrmoptionNoArg, "true" },
  {"+texture_quality", ".concentric.texture", XrmoptionNoArg, "false" },
  {"-texture_quality", ".concentric.texture", XrmoptionNoArg, "true" },
  {"+mipmap",          ".concentric.mipmap",  XrmoptionNoArg, "false" },
  {"-mipmap",          ".concentric.mipmap",  XrmoptionNoArg, "true" },
  {"-name",            ".concentric.name",    XrmoptionSepArg, 0 },
  {"-image",           ".concentric.image",   XrmoptionSepArg, 0 },
};

static argtype vars[] = {
  {&do_light,	 "light",           "Light",           DEF_LIGHT,    t_Bool},
  {&do_texture,	 "texture",         "Texture",         DEF_TEXTURE,  t_Bool},
  {&do_tex_qual, "texture_quality", "Texture_Quality", DEF_TEX_QUAL, t_Bool},
  {&do_mipmap,   "mipmap",          "Mipmap",          DEF_MIPMAP,   t_Bool},
  {&which_name,  "name",            "Name",            DEF_NAME,     t_String},
  {&which_image, "image",           "Image",           DEF_IMAGE,    t_String},
};

static OptionStruct desc[] =
{
  {"-name num", "example 'name' to draw (path)"},
  {"-/+ light", "whether to do enable lighting (slower)"},
  {"-/+ texture", "whether to apply a texture (slower)"},
  {"-image <filename>", "texture image to load"},
  {"-/+ texture_quality", "whether to use texture smoothing (slower)"},
  {"-/+ mipmap", "whether to use texture mipmap (slower)"},
};

ENTRYPOINT ModeSpecOpt concentric_opts = {countof(opts), opts, countof(vars), vars, desc};

#ifdef USE_MODULES
ModStruct   concentric_description =
{"concentric", "init_concentric", "draw_concentric", "release_concentric",
 "draw_concentric", "init_concentric", NULL, &concentric_opts,
 1000, 1, 2, 1, 4, 1.0, "",
 "OpenGL concentric", 0, NULL};
#endif





/* set up a light */
static const GLfloat lightOnePosition[] = {1, 1, 0};
static const GLfloat lightOneAmbient[] = {0, 0, 0, 1};
static const GLfloat lightOneDiffuse[] = {0.75, 0.75, 0.75, 1};
static const GLfloat lightOneSpecular[] = {0.75, 0.75, 0.75, 1};

float rot_x=0, rot_y=0, rot_z=0;
float halfdayfrac=0;
float hourfrac=0;
float minfrac=0;

struct timeval start_time;

/* BEGINNING OF FUNCTIONS */

static void
init_rotation (ModeInfo *mi)
{
  concentric_configuration *gp = &Concentric[MI_SCREEN(mi)];
  double spin_speed = 0.15;
  gp->rot = make_rotator (spin_speed, spin_speed, spin_speed,
                          0.2,
                          0.005,
                          False);
  gp->trackball = gltrackball_init (True);

  gettimeofday(&start_time, NULL);
  halfdayfrac = (start_time.tv_sec % 43200) / 43200.0;
  hourfrac = (start_time.tv_sec % 3600) / 3600.0;
  minfrac = start_time.tv_usec / 60000000.0;
}


/* draw the concentric once */
ENTRYPOINT void
draw_concentric (ModeInfo * mi){
  concentric_configuration *gp = &Concentric[MI_SCREEN(mi)];
  Display    *display = MI_DISPLAY(mi);
  Window      window = MI_WINDOW(mi);

  static const GLfloat color_bg[4] = {0.75, 0.75, 0.75, 1};
  /* silver, from http://devernay.free.fr/cours/opengl/materials.html */
  static const GLfloat color_tubes_amb[4] = {0.19225,0.19225,0.19225, 0.5};
  static const GLfloat color_tubes_dif[4] = {0.50754,0.50754,0.50754, 0.5};
  static const GLfloat color_tubes_spec[4] = {0.508273,0.508273,0.508273, 0.5};
  static const GLfloat color_tubes_shiny = (2); /* 128 * 0.4 */

  int loop;
  int sAng;
  double x, y, z;
  struct timeval cur_time;
  struct GLUquadric* qu;

  gettimeofday(&cur_time, NULL);
  /* for relative time (from start point) */
  /* halfdayfrac = ((cur_time.tv_sec - start_time.tv_sec) % 43200) / 43200.0; */
  /* hourfrac = ((cur_time.tv_sec - start_time.tv_sec) % 3600) / 3600.0; */
  /* minfrac = (float)((((cur_time.tv_sec - start_time.tv_sec) % 120) / 60.0) */
  /*                   + (cur_time.tv_usec / 60000000.0)); */

  /* for absolute time */
  halfdayfrac = ((cur_time.tv_sec) % 43200) / 43200.0;
  hourfrac = ((cur_time.tv_sec) % 3600) / 3600.0;
  minfrac = (float)((((cur_time.tv_sec) % 120) / 60.0)
                    + (cur_time.tv_usec / 60000000.0));

  if(!gp->glx_context)
	return;

  if(do_texture) glEnable(GL_TEXTURE_2D); /* not sure if this is needed -- gringer */

  glXMakeCurrent(MI_DISPLAY(mi), MI_WINDOW(mi), *(gp->glx_context));

  glPushMatrix();

  gltrackball_rotate (gp->trackball);

  get_rotation (gp->rot, &x, &y, &z,
                !(gp->button_down_p || gp->button2_down_p));
  glRotatef (x * 360, 1.0, 0.0, 0.0);
  glRotatef (y * 360, 0.0, 1.0, 0.0);
  glRotatef (z * 360, 0.0, 0.0, 1.0);

  /* track the mouse only if a button is down. */
  if(gp->button2_down_p)
    {
      gp->mouse_dx += gp->mouse_x - gp->mouse_start_x;
      gp->mouse_dy += gp->mouse_y - gp->mouse_start_y;
      gp->mouse_start_x = gp->mouse_x;
      gp->mouse_start_y = gp->mouse_y;
    }

  {
    get_position (gp->rot, &x, &y, &z,
                  !(gp->button_down_p || gp->button2_down_p));
  }

  glScalef(0.5, 0.5, 0.5);

  glFrontFace(GL_CCW);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   /* set up some matrices so that the object spins with the mouse */
   glPushMatrix ();

   /* background logo */
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
   glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color_bg);
   glCallList (gp->platelist);

   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
   /* concentric tubes */
   glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT, color_tubes_amb);
   glMaterialfv (GL_FRONT_AND_BACK, GL_DIFFUSE, color_tubes_spec);
   glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, color_tubes_dif);
   glMaterialf (GL_FRONT_AND_BACK, GL_SHININESS, color_tubes_shiny);
   qu = gluNewQuadric();
   gleSetNumSides(60);
   gleSetJoinStyle (TUBE_NORM_PATH_EDGE | TUBE_JN_ROUND);
   sAng = minfrac * (43200); /* 720 * 60 */
   glRotatef(90 + 360 * halfdayfrac, 0,0,1);
   for(loop = 1; loop <= 6; loop++){
     int loopAngle = (sAng / loop) % 720;
     int loopMod = loopAngle % 360;
     float angDeg = (loop != 6) ? (1.0 * loopMod) : (360.0 * minfrac);
     if(loopAngle != 0){
       if(loop == 6){
         glRotatef(loopAngle, 0,0,1);
       }
       if((loopAngle < 360) && (loop != 6)){
         gleHelicoid(0.75, /* circle contour (torus) radius */
                   2.0 * loop, /* spiral starts in x-y plane (ring radius) */
                   0.0, /* change in radius per revolution */
                   0.0, /* starting z value  */
                   0.0, /* change in Z per revolution  */
                   0x0, /* starting contour affine transformation  */
                   0x0, /* tangent change xform per revolution  */
                   0.0, /* start angle in x-y plane  */
                   angDeg); /* degrees to spiral around */
         glPushMatrix();
         glRotatef(angDeg, 0, 0, 1);
         glTranslatef(2.0 * loop, 0, 0);
         gluSphere(qu, 0.75, 16, 16);
         glPopMatrix();
       } else {
         gleHelicoid(0.75, /* circle contour (torus) radius */
                   2.0 * loop, /* spiral starts in x-y plane (ring radius) */
                   0.0, /* change in radius per revolution */
                   0.0, /* starting z value  */
                   0.0, /* change in Z per revolution  */
                   0x0, /* starting contour affine transformation  */
                   0x0, /* tangent change xform per revolution  */
                   angDeg, /* start angle in x-y plane  */
                   (360.0 - angDeg)); /* degrees to spiral around */
         glPushMatrix();
         glRotatef(angDeg, 0, 0, 1);
         glTranslatef(2.0 * loop, 0, 0);
         gluSphere(qu, 0.75, 16, 16);
         glPopMatrix();
       }
       glPushMatrix();
       glTranslatef(2.0 * loop, 0, 0);
       gluSphere(qu, 0.75, 16, 16);
       glPopMatrix();
     }
   }
   glPopMatrix ();

  glPopMatrix();

  if(mi->fps_p) do_fps (mi);
  glXSwapBuffers(display, window);
}


/* set up lighting conditions */
static void
SetupLight(void)
{
  glLightfv (GL_LIGHT0, GL_POSITION, lightOnePosition);
  glLightfv (GL_LIGHT0, GL_AMBIENT, lightOneAmbient);
  glLightfv (GL_LIGHT0, GL_DIFFUSE, lightOneDiffuse);
  glLightfv (GL_LIGHT0, GL_SPECULAR, lightOneSpecular);

  glEnable (GL_LIGHT0);
  glEnable (GL_LIGHTING);

  glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   glColorMaterial (GL_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable (GL_COLOR_MATERIAL);
}

/* Standard reshape function */
ENTRYPOINT void
reshape_concentric (ModeInfo *mi, int width, int height)
{
  GLfloat h = (GLfloat) height / (GLfloat) width;

  glViewport (0, 0, (GLint) width, (GLint) height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective (30.0, 1/h, 1.0, 100.0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt( 0.0, 0.0, 30.0,
             0.0, 0.0, 0.0,
             0.0, 1.0, 0.0);

  glClear(GL_COLOR_BUFFER_BIT);
}


/* main OpenGL initialization routine */
static void
initializeGL(ModeInfo *mi, GLsizei width, GLsizei height)
{
  reshape_concentric(mi, width, height);
  glViewport( 0, 0, width, height);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glShadeModel(GL_SMOOTH);

# ifdef HAVE_JWZGLES /* #### glPolygonMode other than GL_FILL unimplemented */
  MI_IS_WIREFRAME(mi) = 0;
# endif

  if(do_light)
	SetupLight();
  if(MI_IS_WIREFRAME(mi)) {
	glPolygonMode(GL_FRONT,GL_LINE);
  	glPolygonMode(GL_BACK,GL_LINE);
  }

}

ENTRYPOINT Bool
concentric_handle_event (ModeInfo *mi, XEvent *event)
{
  concentric_configuration *bp = &Concentric[MI_SCREEN(mi)];
  if(event->xany.type == ButtonPress &&
      event->xbutton.button == Button1)
    {
      bp->button_down_p = True;
      gltrackball_start (bp->trackball,
                         event->xbutton.x, event->xbutton.y,
                         MI_WIDTH (mi), MI_HEIGHT (mi));
      return True;
    }
  else if(event->xany.type == ButtonRelease &&
           event->xbutton.button == Button1)
    {
      bp->button_down_p = False;
      return True;
    }
  else if(event->xany.type == ButtonPress &&
           (event->xbutton.button == Button4 ||
            event->xbutton.button == Button5 ||
            event->xbutton.button == Button6 ||
            event->xbutton.button == Button7))
    {
      gltrackball_mousewheel (bp->trackball, event->xbutton.button, 10,
                              !!event->xbutton.state);
      return True;
    }
  else if(event->xany.type == MotionNotify &&
           bp->button_down_p)
    {
      gltrackball_track (bp->trackball,
                         event->xmotion.x, event->xmotion.y,
                         MI_WIDTH (mi), MI_HEIGHT (mi));
      return True;
    }

  return False;

}

/* xconcentric initialization routine */
ENTRYPOINT void
init_concentric (ModeInfo * mi)
{
  int screen = MI_SCREEN(mi);
  concentric_configuration *gp;
  static const GLfloat normalFront[] = {0, 0, 1};
  static const GLfloat normalBack[] = {0, 0, -1};

  if(MI_IS_WIREFRAME(mi)) do_light = 0;

  if(Concentric == NULL) {
	if((Concentric = (concentric_configuration *)
         calloc(MI_NUM_SCREENS(mi), sizeof (concentric_configuration))) == NULL)
	  return;
  }
  gp = &Concentric[screen];

  gp->window = MI_WINDOW(mi);
  if((gp->glx_context = init_GL(mi)) != NULL) {
	reshape_concentric(mi, MI_WIDTH(mi), MI_HEIGHT(mi));
	initializeGL(mi, MI_WIDTH(mi), MI_HEIGHT(mi));
    init_rotation(mi);
  } else {
	MI_CLEARWINDOW(mi);
  }

  /* construct the background image / texture */

  gp->platelist = glGenLists(1);
  glNewList (gp->platelist, GL_COMPILE);
  glColor3f (1,1,1);
  glPushMatrix ();
  glScalef (16.0, 16.0, 16.0);
  glRotatef(180, 0, 0, 1);
  glRotatef(180, 0, 1, 0);
  glFrontFace(GL_CCW);
  glBegin (GL_QUADS);
  glNormal3fv(normalFront);
  glTexCoord2f(0,0); glVertex3f (-1, 1, 0.001);
  glTexCoord2f(1,0); glVertex3f (1, 1, 0.001);
  glTexCoord2f(1,1); glVertex3f (1, -1, 0.001);
  glTexCoord2f(0,1); glVertex3f (-1, -1, 0.001);
  glEnd();
  glBegin (GL_QUADS);
  glNormal3fv(normalBack);
  glTexCoord2f(0,1); glVertex3f (-1, -1, -0.001);
  glTexCoord2f(1,1); glVertex3f (1, -1, -0.001);
  glTexCoord2f(1,0); glVertex3f (1, 1, -0.001);
  glTexCoord2f(0,0); glVertex3f (-1, 1, -0.001);
  glEnd();
  glPopMatrix ();
  glEndList();


}

ENTRYPOINT void
release_concentric (ModeInfo * mi)
{
  int screen;
  for (screen = 0; screen < MI_NUM_SCREENS(mi); screen++) {
    concentric_configuration *gp = &Concentric[screen];
    if (gp->glx_context) {
      /* Display lists MUST be freed while their glXContext is current. */
      /* but this gets a BadMatch error. -jwz */
      /*glXMakeCurrent(MI_DISPLAY(mi), gp->window, *(gp->glx_context));*/

      if (glIsList(gp->platelist))
        glDeleteLists(gp->platelist, 1);
	}
  }
  FreeAllGL(mi);
}

ENTRYPOINT void
free_concentric (ModeInfo * mi){
}


XSCREENSAVER_MODULE ("Concentric", concentric)

#endif  /* USE_GL */
