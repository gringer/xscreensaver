
/* colclock -- an analog clock with colour-changing hands
 * Copyright (c) 2020 David Eccles <bioinformatics@gringene.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 *
 * Derived from AnClock by PTK, with these additions:
 * - added colour dots at ends of hands
 * - sine-wave "tick" motion for high update frequencies
 * - added numbers / font loading
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include "screenhack.h"
#include "hsv.h"

typedef int               boolean;
typedef unsigned short    ushort;
typedef unsigned long     ulong;
typedef XWindowAttributes XGWA;

static const char *colclock_defaults [] = {
  ".background:   black",
  ".foreground:   white",
  "*brightness:   70.0",  /* [%]  */
  ".color:        True",
  "*frequency:    10",     /* [Hz] */
  "*font:         -*-bitstream charter-*-r-*-*-40-0-0-0-*-*-*-*",
  "*grayscale:    False",
  "*pin:          True",
  "*size:         95.0",  /* [%]  */
  "*tick:         True",
  "*wireframe:    False",
  "*hide-index:   False",
  "*hide-mark:    False",
  "*hide-second:  False",
  0
};

static XrmOptionDescRec colclock_options [] = {
  { "-background" ,  ".background" ,  XrmoptionSepArg,  0      },
  { "-foreground" ,  ".foreground" ,  XrmoptionSepArg,  0      },
  { "-brightness" ,  ".brightness" ,  XrmoptionSepArg,  0      },
  { "-colorless"  ,  ".color"      ,  XrmoptionNoArg ,  "False"},
  { "-frequency"  ,  ".frequency"  ,  XrmoptionSepArg,  0      },
  { "-font"       ,  ".font"       ,  XrmoptionSepArg,  0      },
  { "-grayscale"  ,  ".grayscale"  ,  XrmoptionNoArg ,  "True" },
  { "-pin"        ,  ".pin"        ,  XrmoptionNoArg ,  "True" },
  { "-size"       ,  ".size"       ,  XrmoptionSepArg,  0      },
  { "-tickless"   ,  ".tick"       ,  XrmoptionNoArg ,  "False"},
  { "-wireframe"  ,  ".wireframe"  ,  XrmoptionNoArg ,  "True" },
  { "-hide-index" ,  ".hide-index" ,  XrmoptionNoArg ,  "True" },
  { "-hide-mark"  ,  ".hide-mark"  ,  XrmoptionNoArg ,  "True" },
  { "-hide-second",  ".hide-second",  XrmoptionNoArg ,  "True" },
  { 0, 0, 0, 0 }
};

/* type definitions */
enum HAND_TYPE {htSECOND, htMINUTE, htHOUR, htMAX};
enum HAND_VERTEX_TYPE {hvAPEX, hvRIGHT, hvBOTTOM, hvLEFT, hvMAX};

/* parameter settings */

/* ratio definitions [%] */
static const double font_ratio          =  150;
static const double font_radius         =  90;
static const double mark_outer_ratio    =  77;
static const double mark_inner_ratio    =  73;
static const double index_outer_ratio   =  77;
static const double index_inner_ratio   =  69;
static const double hand_radius_ratios[htMAX][hvMAX] = {
  {75,  68, 20, 29}, /* Sec  */
  {65,  53, 10,  3}, /* Min  */
  {45,  30, 10,  4}  /* Hour */
};
static const double center_circle_ratio  =  3;
static const double second_blob_ratio  =  4;

/* angle definitions [degree] */
static const double index_arc_angle = 0.6;
static const double hand_vertex_angles[htMAX][hvMAX] = {
  {0,  0, 180, 182}, /* Sec  */
  {0,  0, 180, 306}, /* Min  */
  {0,  0, 180, 306}  /* Hour */
};

/* color range settings */
static ushort high_color_default = 0xF000;
static ushort mid_color_default  = 0xE000;
static ushort low_color_default  = 0xD000;
static ushort color_step_default = 0x0010;

/* function macros */
#define i_random(i) ((int)(((double)i)*random()/RAND_MAX))
#define i_round(x)  ((int)(((double)x)+0.5))
#define d_sin(x)    (sin(2*M_PI*(x)/360))
#define d_cos(x)    (cos(2*M_PI*(x)/360))
#define min(x,y)    (((x)<(y))?(x):(y))
#define rlogt(x)    ((pow(10,x) - 1) / 9)
#define abs(x)      ((x)>0?(x):(-(x)))

typedef struct {
  int x, y;
} XY;

typedef struct {
  Display   *dpy;
  Window     window;
  XGWA       xgwa;
  Pixmap     pixmap;
  XFontStruct *font;
  boolean    scaledFont;
  int        fWidth;
  int        fHeight;
  GC         bg_gc, face_gc, line_gc, hourTick_gc, minTick_gc, hand_gc[htMAX], handLine_gc[htMAX];
  XColor     bg_color, fg_color, hand_color[htMAX];
  int        width, height;
  double     radius;
  XY         center;
  XY         orient;
  ulong      delay; /* [usec] */
  ushort     high_color;
  ushort     mid_color;
  ushort     low_color;
  ushort     color_step;
  ushort     diff_colors[3];

  /* commandline options */
  double     brightness;
  boolean    color;
  char*      fontName;
  int        frequency;
  boolean    grayscale;
  boolean    mono;
  boolean    pin;
  boolean    tick;
  double     size;
  boolean    wireframe;
  boolean    hide_index;
  boolean    hide_mark;
  boolean    hide_second;
} state;


static void set_gc_color (state *st, GC gc, ushort fr, ushort fg, ushort fb)
{
  XColor fg_color;

  fg_color.red   = fr;
  fg_color.green = fg;
  fg_color.blue  = fb;

  XAllocColor (st->dpy, st->xgwa.colormap, &fg_color);
  XSetForeground (st->dpy, gc, fg_color.pixel);
}

static void set_fg_color (state *st, ushort r, ushort g, ushort b)
{
  ushort fr, fg, fb, lr, lg, lb;
  int ht;

  if (st->mono)
    if (st->wireframe) {
      fr = fg = fb = 0;
      lr = lg = lb = 0xFFFF;
    }
    else {
      fr = fg = fb = 0xFFFF;
      lr = lg = lb = 0xFFFF;
    }
  else
    if (st->wireframe) {
      fr = st->bg_color.red; fg = st->bg_color.green; fb = st->bg_color.blue;
      lr = r; lg = g; lb = b;
    }
    else {
      fr = r; fg = g; fb = b;
      lr = r / 2; lg = g / 2; lb = b / 2;
    }

  set_gc_color (st, st->face_gc, fr, fg, fb);
  set_gc_color (st, st->line_gc, lr, lg, lb);
  set_gc_color (st, st->minTick_gc, fr, fg, fb);
  set_gc_color (st, st->hourTick_gc, lr, lg, lb);
  for(ht = 0; ht < htMAX; ht++){
    set_gc_color (st, st->handLine_gc[ht],
                  lr, lb, lg);
  }

  if (st->wireframe) {
    fr = lr; fg = lg; fb = lb;
  }
  st->fg_color.red   = fr;
  st->fg_color.green = fg;
  st->fg_color.blue  = fb;
}

static void change_color (state *st)
{
  ushort temp_colors[3], buff;
  int    color_orient;
  int    i, j;

  if (st->mid_color == st->fg_color.red) {
    color_orient = i_random (2) * 2 - 1;
    for (i = 0; i < 3; i++)
      st->diff_colors[i] = st->color_step * color_orient;
    st->diff_colors[i_random (3)] = -2 * st->color_step * color_orient;
  }

  temp_colors[0] = st->fg_color.red;
  temp_colors[1] = st->fg_color.green;
  temp_colors[2] = st->fg_color.blue;
  for (i = 0; i < 3; i++) {
    buff = temp_colors[i] + st->diff_colors[i];
    if ((buff < st->low_color) || (st->high_color < buff)) {
      for (j = 0; j < 3; j++)
        st->diff_colors[j] = -st->diff_colors[j];
      break;
    }
  }

  for (i = 0; i < 3; i++)
    temp_colors[i] += st->diff_colors[i];

  set_fg_color (st, temp_colors[0], temp_colors[1], temp_colors[2]);
}

static void draw_indexes (state *st)
{
  int      hour, min, hLen;
  double   radii[4];
  double   theta;
  double   vertex_angles[4];
  double   sin_theta, cos_theta;
  char     numStr[3];
  int      x1,y1,x2,y2;
  boolean  hourMark;

  radii[0] = st->radius * index_outer_ratio / 100;
  radii[1] = st->radius * index_inner_ratio / 100;
  radii[2] = st->radius * mark_outer_ratio / 100;
  radii[3] = st->radius * mark_inner_ratio / 100;
  vertex_angles[0] = vertex_angles[1] =  index_arc_angle;
  vertex_angles[2] = vertex_angles[3] = -index_arc_angle;

  for (min = 0; min < 60; min ++) {
    theta = min * 6;
    hourMark = ((min % 5) == 0);
    sin_theta = d_sin (theta);
    cos_theta = d_cos (theta);
    x1 = i_round(st->center.x + (hourMark?radii[0]:radii[2]) * sin_theta);
    x2 = i_round(st->center.x + (hourMark?radii[1]:radii[3]) * sin_theta);
    y1 = i_round(st->center.y - (hourMark?radii[0]:radii[2]) * cos_theta);
    y2 = i_round(st->center.y - (hourMark?radii[1]:radii[3]) * cos_theta);

    if(hourMark && (!st->hide_mark)){
      XCharStruct overall;
      int dir, ascent, descent, hWidth, hHeight;
      hour = (min == 0)?12:(min / 5);
      sprintf(numStr, "%d", hour);
      hLen = (hour > 9) ? 2 : 1;
      XTextExtents (st->font, numStr, hLen, &dir, &ascent,
                    &descent, &overall);
      hWidth = (overall.rbearing - overall.lbearing) / 2;
      hHeight = (overall.ascent + overall.descent) / 2;

      XDrawString (st->dpy, st->pixmap, st->hourTick_gc,
                   st->center.x + st->radius * font_radius / 100 * d_sin (theta) - hWidth,
                   st->center.y - st->radius * font_radius / 100 * d_cos (theta) + hHeight,
                   numStr, hLen);
      XDrawLine(st->dpy, st->pixmap, st->hourTick_gc, x1, y1, x2, y2);
    } else if (!st->hide_index) {
      XDrawLine(st->dpy, st->pixmap, st->minTick_gc, x1, y1, x2, y2);
    }
  }
}

static void draw_circle(state *st, double circle_radius, int cx, int cy, GC fill_gc, GC stroke_gc){
  static const int twi_pi = 360 * 64;
  int    x, y, w, h;
  x = i_round (cx - circle_radius);
  y = i_round (cy - circle_radius);
  w = h = i_round (circle_radius * 2);
  XFillArc (st->dpy, st->pixmap, fill_gc, x, y, w, h, 0, twi_pi);
  XDrawArc (st->dpy, st->pixmap, stroke_gc, x, y, w, h, 0, twi_pi);
}

static void draw_unfilled_circle(state *st, double circle_radius, int cx, int cy, GC stroke_gc){
  static const int twi_pi = 360 * 64;
  int    x, y, w, h;
  x = i_round (cx - circle_radius);
  y = i_round (cy - circle_radius);
  w = h = i_round (circle_radius * 2);
  XDrawArc (st->dpy, st->pixmap, stroke_gc, x, y, w, h, 0, twi_pi);
}

static void draw_hand (state *st,enum HAND_TYPE ht, double theta)
/* theta [degree] */
{
  enum HAND_VERTEX_TYPE hv;
  double   hand_radius;
  XPoint   hand_vertexes[hvMAX + 1];
  /* hand */
  for (hv = 0; hv < hvMAX; hv++) {
    hand_radius = st->radius * hand_radius_ratios[ht][hv] / 100;
    hand_vertexes[hv].x =
      i_round (st->center.x +
               hand_radius *
               d_sin (theta + hand_vertex_angles[ht][hv]));
    hand_vertexes[hv].y =
      i_round (st->center.y -
               hand_radius *
               d_cos (theta + hand_vertex_angles[ht][hv]));
  }
  hand_vertexes[hvMAX].x = hand_vertexes[0].x;
  hand_vertexes[hvMAX].y = hand_vertexes[0].y;

  XDrawLine(st->dpy, st->pixmap, st->handLine_gc[ht],
            hand_vertexes[0].x, hand_vertexes[0].y,
            hand_vertexes[2].x, hand_vertexes[2].y);

  if(st->color){
    draw_circle(st, st->radius * (second_blob_ratio / 100) * (ht + 1),
                hand_vertexes[1].x, hand_vertexes[1].y,
                st->hand_gc[ht], st->handLine_gc[ht]);
  }
}

static void draw_center_circle (state *st)
{
  draw_circle(st, st->radius * center_circle_ratio / 100, st->center.x, st->center.y,
              st->face_gc, st->line_gc);
  draw_unfilled_circle(st, st->radius * (mark_outer_ratio+3)/100,
                       st->center.x, st->center.y, st->line_gc);
  draw_unfilled_circle(st, st->radius, st->center.x, st->center.y, st->line_gc);
}

static XColor get_angle_color (double theta){
  /* top: red, right: yellow, bottom: [dark] green, left: blue */
  double h = 0;
  double s,v;
  ushort r,g,b;
  XColor color;
  s = 1.0; v = 1.0;
  if        ((theta >=   0) && (theta <  90)) {
    h = (theta * (60.0 / 90.0));
  } else if ((theta >=  90) && (theta < 180)) {
    h = ((theta - 90.0) * (60.0 / 90.0)) + 60.0;
    v = 1.0 - ((theta - 90.0) / 180.0);
  } else if ((theta >= 180) && (theta < 270)) {
    h = ((theta - 180.0) * (120.0 / 90.0)) + 120.0;
    v = ((theta - 180.0) / 180.0) + 0.5;
  } else if ((theta >= 270) && (theta < 360)) {
    h = ((theta - 270.0) * (120.0 / 90.0)) + 240.0;
  }
  hsv_to_rgb((int)h,s,v,&r,&g,&b);
  color.red = r;
  color.green = g;
  color.blue = b;
  return color;
}

static void draw_clock_pixmap (state *st)
{
  struct timeval  tv;
  struct tm      *now;
  suseconds_t     msec;
  double          numerator, denominator;
  double          theta[htMAX];
  XColor          c;

  gettimeofday (&tv, NULL);
  now = localtime (&tv.tv_sec);
  msec = tv.tv_usec / st->delay * st->delay / 1000;

  numerator       = 1000 * now->tm_sec + msec;
  denominator     = 60 * 1000;
  theta[htSECOND] = 360 * numerator / denominator;
  numerator       = 60 * 1000 * now->tm_min + numerator;
  denominator     = 60 * denominator;
  theta[htMINUTE] = 360 * numerator / denominator;
  numerator       = 60 * 60 * 1000 * (now->tm_hour % 12) + numerator;
  denominator     = 12 * denominator;
  theta[htHOUR]   = 360 * numerator / denominator;

  XFillRectangle (st->dpy, st->pixmap, st->bg_gc, 0, 0, st->width, st->height);

  draw_indexes(st);
  if(st->color){
    c = get_angle_color(theta[htHOUR]);
    set_gc_color(st, st->hand_gc[htHOUR], c.red, c.green, c.blue);
    c = get_angle_color(theta[htMINUTE]);
    set_gc_color(st, st->hand_gc[htMINUTE], c.red, c.green, c.blue);
    c = get_angle_color(theta[htSECOND]);
    set_gc_color(st, st->hand_gc[htSECOND], c.red, c.green, c.blue);
  }
  if(st->tick){
    double hFrac, hInt;
    hFrac = modf(theta[htSECOND] / 6, &hInt);
    if(st->frequency > 1){
      hFrac = (1.0 - sin((0.5-hFrac) * M_PI))/2.0;
      theta[htSECOND] = (hInt + hFrac) * 6;
    } else {
      theta[htSECOND] = hInt * 6;
    }
    hFrac = modf(theta[htMINUTE] / 6, &hInt);
    theta[htMINUTE] = hInt * 6;
  }
  draw_hand (st, htHOUR, theta[htHOUR]);
  draw_hand (st, htMINUTE, theta[htMINUTE]);
  if (!st->hide_second){
    draw_hand (st, htSECOND, theta[htSECOND]);
  }
  draw_center_circle (st);
}

static void move_clock (state *st)
{
  if (st->center.x - st->radius <= 0)
    st->orient.x =  1;
  if (st->width <= st->center.x + st->radius)
    st->orient.x = -1;
  if (st->center.y - st->radius <= 0)
    st->orient.y =  1;
  if (st->height <= st->center.y + st->radius)
    st->orient.y = -1;
  st->center.x += st->orient.x;
  st->center.y += st->orient.y;
}

static void create_pixmap (state *st)
{
  if (st->pixmap)
    XFreePixmap (st->dpy, st->pixmap);
  st->pixmap = XCreatePixmap (st->dpy, st->window, st->width, st->height,
                              st->xgwa.depth);
}

static void option_warning (char *opt, char *val, char *msg, char *newval)
{
  fprintf (stderr, "Warning: %s %s is %s: using %s.\n",
           opt, val, msg, newval);
}

static void correct_options (state *st)
{
  char    val[256], newval[256];
  char   *range_msg      = "out of range";
  char   *odd_msg        = "odd";
  char   *opt_brightness = "brightness";
  char   *opt_frequency  = "frequency";
  char   *opt_size       = "size";
  char   *fmt_brightness = "%.1f";
  char   *fmt_frequency  = "%d";
  char   *fmt_size       = "%.1f";
  double  min_brightness =  10.0;
  double  max_brightness = 100.0;
  int     min_frequency  =   1;
  int     max_frequency  =  10;
  double  min_size       =  10.0;
  double  max_size       =  99.0;

  sprintf (val, fmt_brightness, st->brightness);
  if (st->brightness < min_brightness) {
    sprintf (newval, fmt_brightness, min_brightness);
    option_warning (opt_brightness, val, range_msg, newval);
    st->brightness = min_brightness;
  }
  if (max_brightness < st->brightness) {
    sprintf (newval, fmt_brightness, max_brightness);
    option_warning (opt_brightness, val, range_msg, newval);
    st->brightness = max_brightness;
  }

  sprintf (val, fmt_frequency, st->frequency);
  if (st->frequency < min_frequency) {
    sprintf (newval, fmt_frequency, min_frequency);
    option_warning (opt_frequency, val, range_msg, newval);
    st->frequency = min_frequency;
  }
  if (max_frequency < st->frequency) {
    sprintf (newval, fmt_frequency, max_frequency);
    option_warning (opt_frequency, val, range_msg, newval);
    st->frequency = max_frequency;
  }
  if (0 != 1000 % st->frequency) {
    sprintf (newval, fmt_frequency, 5);
    option_warning (opt_frequency, val, odd_msg, newval);
    st->frequency = 5;
  }

  sprintf (val, fmt_size, st->size);
  if (st->size < min_size) {
    sprintf (newval, fmt_size, min_size);
    option_warning (opt_size, val, range_msg, newval);
    st->size = min_size;
  }
  if (max_size < st->size) {
    sprintf (newval, fmt_size, max_size);
    option_warning (opt_size, val, range_msg, newval);
    st->size = max_size;
  }
}

static void *colclock_init (Display *dpy, Window window)
{
  state *st = (state *) calloc (1, sizeof (*st));
  ulong pixel;
  int ht;

  st->dpy = dpy;
  st->window = window;
  XGetWindowAttributes (st->dpy, st->window, &st->xgwa);
  st->width  = st->xgwa.width;
  st->height = st->xgwa.height;

  st->brightness  = get_float_resource   (dpy, "brightness" , "Float"  );
  st->color       = get_boolean_resource (dpy, "color"      , "Boolean");
  st->fontName    = get_string_resource  (dpy, "font"       , "Font");
  st->frequency   = get_integer_resource (dpy, "frequency"  , "Integer");
  st->grayscale   = get_boolean_resource (dpy, "grayscale"  , "Boolean");
  st->mono        = get_boolean_resource (dpy, "mono"       , "Boolean");
  st->pin         = get_boolean_resource (dpy, "pin"        , "Boolean");
  st->size        = get_float_resource   (dpy, "size"       , "Float"  );
  st->tick        = get_boolean_resource (dpy, "tick"       , "Boolean");
  st->wireframe   = get_boolean_resource (dpy, "wireframe"  , "Boolean");
  st->hide_index  = get_boolean_resource (dpy, "hide-index" , "Boolean");
  st->hide_mark   = get_boolean_resource (dpy, "hide-mark"  , "Boolean");
  st->hide_second = get_boolean_resource (dpy, "hide-second", "Boolean");
  correct_options (st);
  st->delay = 1000000 / st->frequency;

  st->high_color = i_round (high_color_default * st->brightness / 100);
  st->mid_color  = i_round (mid_color_default  * st->brightness / 100);
  st->low_color  = i_round (low_color_default  * st->brightness / 100);
  st->color_step = i_round (color_step_default * st->brightness / 100);

  st->bg_gc   = XCreateGC (dpy, window, 0, NULL);
  st->face_gc = XCreateGC (dpy, window, 0, NULL);
  st->line_gc = XCreateGC (dpy, window, 0, NULL);
  st->minTick_gc = XCreateGC (dpy, window, 0, NULL);
  st->hourTick_gc = XCreateGC (dpy, window, 0, NULL);
  for(ht = htSECOND; ht < htMAX; ht++){
    st->hand_gc[ht] = XCreateGC (dpy, window, 0, NULL);
    st->handLine_gc[ht] = XCreateGC (dpy, window, 0, NULL);
    XSetLineAttributes(st->dpy, st->handLine_gc[ht], (ht+1)*2, LineSolid, CapRound, JoinRound);
  }

  if (st->mono)
    pixel = BlackPixel (dpy, 0);
  else
    pixel = get_pixel_resource (dpy, st->xgwa.colormap,
                                "background", "Background");
  st->bg_color.pixel = pixel;
  XQueryColor (dpy, st->xgwa.colormap, &st->bg_color);
  set_gc_color (st, st->bg_gc,
                st->bg_color.red, st->bg_color.green, st->bg_color.blue);
  set_fg_color (st, st->mid_color, st->mid_color, st->mid_color);
  XSetLineAttributes(st->dpy, st->minTick_gc, 3, LineSolid, CapRound, JoinRound);
  XSetLineAttributes(st->dpy, st->hourTick_gc, 5, LineSolid, CapRound, JoinRound);

  st->radius = min (st->width, st->height) / 2.0 * st->size / 100;
  if (st->pin) {
    st->center.x = i_round (st->width  / 2.0);
    st->center.y = i_round (st->height / 2.0);
  }
  else {
    st->center.x = i_round (st->radius) +
      i_random (i_round (st->width  - st->radius * 2));
    st->center.y = i_round (st->radius) +
      i_random (i_round (st->height - st->radius * 2));
  }
  st->orient.x = i_random (2) * 2 - 1;
  st->orient.y = i_random (2) * 2 - 1;

  /* messy font loading stuff (from xlockmore.c) */
  {
    char *name = get_string_resource (dpy, "font", "Font");
    st->scaledFont = True;
    if (name)
      {
        char* convertedName = (char *) malloc (256);
        XFontStruct *f;
        const char *def1 = 
          "-*-bitstream charter-*-r-*-*-0-0-0-0-*-*-*-*";
        const char *def2 = "fixed";
        sprintf(convertedName, name, i_round(st->radius * (font_ratio / 100)));
        f = XLoadQueryFont (dpy, convertedName);
        free(convertedName);
        if (!f)
          {
            fprintf (stderr, "%s: font %s does not exist, using %s\n",
                     progname, name, def1);
            f = XLoadQueryFont (dpy, def1);
          } else {
          st->scaledFont = True;
        }
        if (!f)
          {
            fprintf (stderr, "%s: font %s does not exist, using %s\n",
                     progname, def1, def2);
            f = XLoadQueryFont (dpy, def2);
          }
        if (f){
          XCharStruct overall;
          int dir, ascent, descent;
          st->font = f;
          XSetFont (dpy, st->hourTick_gc, f->fid);
          XTextExtents (st->font, "X", 1, &dir, &ascent,
                        &descent, &overall);
          st->fWidth = overall.rbearing - overall.lbearing;
          st->fHeight = overall.ascent + overall.descent;
        }
        free (name);
      }
  }


  create_pixmap (st);

  return st;
}

static void colclock_reshape (Display *dpy, Window window, void *closure,
                             unsigned int w, unsigned int h)
{
  state *st = (state *) closure;

  XGetWindowAttributes (st->dpy, st->window, &st->xgwa);
  st->width  = st->xgwa.width;
  st->height = st->xgwa.height;
  st->radius = min (st->width, st->height) / 2.0 * st->size / 100;

  /* messy font loading stuff (from xlockmore.c) */
  if(st->scaledFont){
    char *name = get_string_resource (dpy, "font", "Font");
    char* convertedName = (char *) malloc (256);
    XFontStruct *f;
    sprintf(convertedName, name, i_round(st->radius * (font_ratio / 100)));
    f = XLoadQueryFont (dpy, convertedName);
    free(name);
    free(convertedName);
    if (f){
      XCharStruct overall;
      int dir, ascent, descent;
      XFreeFont(dpy, st->font);
      st->font = f;
      XSetFont (dpy, st->hourTick_gc, f->fid);
      XTextExtents (st->font, "X", 1, &dir, &ascent,
                    &descent, &overall);
      st->fWidth = overall.rbearing - overall.lbearing;
      st->fHeight = overall.ascent + overall.descent;
    }
  }

  if (st->pin) {
    st->center.x = i_round (st->width  / 2.0);
    st->center.y = i_round (st->height / 2.0);
  }
  else {
    if (st->center.x - st->radius < 0)
      st->center.x = i_round (st->radius);
    if (st->width < st->center.x + st->radius)
      st->center.x = st->width - i_round (st->radius);
    if (st->center.y - st->radius < 0)
      st->center.y = i_round (st->radius);
    if (st->height < st->center.y + st->radius)
      st->center.y = st->height - i_round (st->radius);
  }

  create_pixmap (st);
}

static Bool colclock_event (Display *dpy, Window window, void *closure,
                           XEvent *event)
{
  return False;
}

static ulong colclock_draw (Display *dpy, Window window, void *closure)
{
  state *st = (state *) closure;
  struct timeval tv;

  if (!st->pin)
    move_clock (st);
  if (!st->grayscale && !st->mono)
    change_color (st);
  draw_clock_pixmap (st);
  XCopyArea (st->dpy, st->pixmap, st->window, st->bg_gc,
             0, 0, st->width, st->height, 0, 0);
  gettimeofday (&tv, NULL);
  return (st->delay - tv.tv_usec % st->delay);
}

static void colclock_free (Display *dpy, Window window, void *closure)
{
  state *st = (state *) closure;
  int ht;

  XFreeFont(dpy, st->font);
  XFreePixmap (st->dpy, st->pixmap);
  XFreeGC (st->dpy, st->bg_gc);
  XFreeGC (st->dpy, st->face_gc);
  XFreeGC (st->dpy, st->line_gc);
  XFreeGC (st->dpy, st->minTick_gc);
           XFreeGC (st->dpy, st->hourTick_gc);
  for(ht = htSECOND; ht < htMAX; ht++){
    XFreeGC (st->dpy, st->hand_gc[ht]);
    XFreeGC (st->dpy, st->handLine_gc[ht]);
  }
  free (st);
}

XSCREENSAVER_MODULE ("Colour Clock", colclock)
