.TH XScreenSaver 1 "20-Mar-2009" "X Version 11"
.SH NAME
colclock - a clock with colour-changing hands
.SH SYNOPSIS
.B anclock
[\-root]
[\-window]
[\-mono]
[\-install]
[\-noinstall]
[\-visual \fIvisual\fP]
[\-window-id \fIid\fP]
[\-fps]
[\-no-fps]
[\-pair]
[\-background \fIspec\fP]
[\-colorless]
[\-brightness \fIfrac\fP]
[\-foreground \fIspec\fP]
[\-frequency \fIint\fP]
[\-grayscale]
[\-pin]
[\-size \fIpercent\fP]
[\-tickless]
[\-wireframe]
[\-hide-index]
[\-hide-mark]
[\-hide-second]
.SH DESCRIPTION
The \fIcolclock\fP program draws a clock with colour-changing hands.
.SH OPTIONS
.I anclock
accepts the following options:
.TP 8
.B \-root
Draw on the root window.
.TP 8
.B \-window
Draw on a newly-created window.  This is the default.
.TP 8
.B \-mono
Monochrome clock (2-bit).
.TP 8
.B \-install
Install a private colormap for the window.  Currently, it doesn't work.
.TP 8
.B \-noinstall
Don't install a private colormap for the window.
.TP 8
.B \-visual \fIvisual\fP
Specify which visual to use.  Legal values are the name of a visual class,
or the id number (decimal or hex) of a specific visual.
.TP 8
.B \-window-id \fIid\fP
Draw to the indicated window instead; this only works if the
.BR xscreensaver-getimage (1)
program is installed.
.TP 8
.B \-fps
Display the current frame rate and CPU load.
.TP 8
.B \-no-fps
Don't display fps.  This is the default.
.TP 8
.B \-pair
Create a pair of windows.
.TP 8
.B \-background(-bg) \fIspec\fP
What to use for the background.  This may be a color name,
a hexadecimal RGB specification in the form '#rrggbb',
or the name of a PPM file.  Default black.
.TP 8
.B \-brightness \fIfrac\fP
Relative brightness ratio [%].  Default 70.
.TP 8
.B \-colorless
Don't use dynamically changing color dots on end of hands.
.TP 8
.B \-foreground(-fg) \fIspec\fP
What to use for the fps font color.  This may be a color name,
a hexadecimal RGB specification in the form '#rrggbb',
or the name of a PPM file.  Default white.
.TP 8
.B \-frequency \fIint\fP
Vibration frequency [Hz].  Legal values are 1, 2, 4, 5, 8 or 10.  Default 5.
.TP 8
.B \-grayscale
Grayscale clock (no color changing).
.TP 8
.B \-pin
Fix clock on the center of the screen.
.TP 8
.B \-tickless
Use a linear sub-second hand movement.
.TP 8
.B \-size \fIfrac\fP
Clock size ratio [%].  Default 85.
.TP 8
.B \-wireframe
Wireframe clock.
.TP 8
.B \-hide-index
Don't show the indexes.
.TP 8
.B \-hide-mark
Don't show the second marks.
.TP 8
.B \-hide-second
Don't show the second hand.
.SH ENVIRONMENT
.PP
.TP 8
.B DISPLAY
to get the default host and display number.
.TP 8
.B XENVIRONMENT
to get the name of a resource file that overrides the global resources
stored in the RESOURCE_MANAGER property.
.SH SEE ALSO
.BR X (1),
.BR xscreensaver (1)
.SH COPYRIGHT
Copyright \(co 2009 by David Eccles (gringer).  Permission to use, copy, modify, 
distribute, and sell this software and its documentation for any purpose is 
hereby granted without fee, provided that the above copyright notice appear 
in all copies and that both that copyright notice and this permission notice
appear in supporting documentation.  No representations are made about the 
suitability of this software for any purpose.  It is provided "as is" without
express or implied warranty.
.SH AUTHOR
David Eccles (gringer) <bioinformatics@gringene.org>, 02-Feb-2020.
